import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('snmp_lcr_hosts')


def test_config_file_exists(host):
    file = host.file('/etc/snmp/snmpd.conf')
    assert file.exists
    assert file.mode == 0o600
    assert file.user == 'root'


def test_snmpd_enabled_and_running(host):
    service = host.service('snmpd')
    assert service.is_running
    assert service.is_enabled
