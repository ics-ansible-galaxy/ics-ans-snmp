ics-ans-snmp
===================

Ansible playbook to install snmp.

Requirements
------------

- ansible >= 2.4
- molecule >= 2.6

License
-------

BSD 2-clause
